package com.chikieblan4ik.denisireliagamescount;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "main";

    Summoner summoner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Map<String, String> keysValues = new HashMap<>();
        keysValues.put("api_key", Api.apiKey);

        String responseStr = Api.request(keysValues, "GET", "lol/summoner/v4/summoners/by-name/D3PII");
        //JSONObject aye = Api.request(keysValues, "GET", "lol/league/v4/entries/RANKED_SOLO_5x5/DIAMOND/I?");
        //JSONArray aye = Api.request(keysValues, "GET", "lol/league/v4/entries/RANKED_SOLO_5x5/DIAMOND/I?");
        Log.i(TAG, "onCreate123: " + responseStr);

        try {
            JSONObject response = new JSONObject(responseStr);
            summoner = new Summoner(response.getString("id"), response.getString("accountId"), response.getString("puuid"), response.getString("name"));
            Log.i(TAG, "123123123: " + response.getInt("code"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}