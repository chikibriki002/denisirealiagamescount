package com.chikieblan4ik.denisireliagamescount;

import android.os.Parcel;
import android.os.Parcelable;

public class Summoner implements Parcelable {

    private String id;
    private String accountId;
    private String puuid;
    private String name;

    public Summoner(String id, String accountId, String puuid, String name) {
        this.id = id;
        this.accountId = accountId;
        this.puuid = puuid;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Summoner{" +
                "id=" + id + '\'' +
                ", accountId=" + accountId + '\'' +
                ", puuid=" + puuid + '\'' +
                ", name=" + name + '\'' +
                '}';
    }

    protected Summoner(Parcel in) {
        id = in.readString();
        accountId = in.readString();
        puuid = in.readString();
        name = in.readString();
    }

    public static final Creator<Summoner> CREATOR = new Creator<Summoner>() {
        @Override
        public Summoner createFromParcel(Parcel in) {
            return new Summoner(in);
        }

        @Override
        public Summoner[] newArray(int size) {
            return new Summoner[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(accountId);
        dest.writeString(puuid);
        dest.writeString(name);
    }

    public String getId() {
        return id;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getPuuid() {
        return puuid;
    }

    public String getName() {
        return name;
    }
}
