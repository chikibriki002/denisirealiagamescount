package com.chikieblan4ik.denisireliagamescount;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.Buffer;
import java.util.Iterator;
import java.util.Map;

public class Api {

    private static final String TAG = "api";

    private static String url = "https://ru.api.riotgames.com/";
    private static String url1 = "https://reqres.in/api/"; // начало ссылки

    public static String apiKey = "RGAPI-a6809f03-8208-4b8f-a5c0-20c6721f2086";

    public static String responseFromStream(BufferedReader in) {
        try {
            StringBuilder builder = new StringBuilder();
            String temp;

            while ((temp = in.readLine()) != null) {
                builder.append(temp);
            }
            Log.i(TAG, "responseFromStream: " + builder.toString());
            return builder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "responseFromStream: " + e);
            return null;
        }
    }

    public static String getUrlParams(Map<String, String> keysValues, String method) {
        try {
            switch (method) {
                case "PUT":
                case "POST":
                    return new JSONObject(keysValues).toString();
                case "GET":
                    StringBuilder constructor = new StringBuilder();
                    Iterator iterator = keysValues.keySet().iterator();

                    while (iterator.hasNext()) {
                        String key = iterator.next().toString();
                        constructor.append(key);
                        constructor.append("=");
                        constructor.append(keysValues.get(key));
                        constructor.append("&");
                    }

                    return constructor.substring(0, constructor.length() - 1);
                default:
                    return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getUrlParams: " + e);
            return null;
        }
    }

    static String response;
    public static String request(final Map<String, String> keysValues, final String capsTypeRequest, final String methodApi) {
        response = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    String myUrl = url + methodApi;
                    if (capsTypeRequest.equals("DELETE")) {
                        myUrl = myUrl + "/" + keysValues.get("id");
                    }
                    if (capsTypeRequest.equals("GET")) {
                        myUrl = myUrl + "?" + getUrlParams(keysValues, capsTypeRequest);
                    }

                    HttpURLConnection connection = (HttpURLConnection) new URL(myUrl).openConnection();
                    if (!capsTypeRequest.equals("GET")) {
                        connection.setRequestProperty("Content-type", "application/json; charset=utf-8");
                        connection.setRequestMethod(capsTypeRequest);
                        connection.setDoOutput(true);
                    }
                    if (!capsTypeRequest.equals("DELETE") & !capsTypeRequest.equals("GET")) {
                        connection.getOutputStream().write(getUrlParams(keysValues, capsTypeRequest).getBytes());
                    }
                    Log.i(TAG, "code: " + connection.getResponseCode());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    //response = new JSONObject(responseFromStream(in));
                    //response = responseFromStream(in);
                    String temp = responseFromStream(in);
                    response = temp.substring(0, temp.length() - 1) + ",\"code\":" + connection.getResponseCode() + "}";
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "runRequest: " + e);
                }
            }
        };

        Thread thread = new Thread(run);
        try {
            thread.start();
            thread.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "requestThread: " + e);
        }
        return response;
    }
}


//                    if (capsTypeRequest.equals("GET")) {
//                        String myUrl = url + methodApi + getUrlParams(keysValues, capsTypeRequest);
//                        HttpURLConnection connection = (HttpURLConnection) new URL(myUrl).openConnection();
//                        BufferedReader in1 = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                        response = responseFromStream(in1);
//                    }
//
